import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import Dashboard from "./dashboard";

let container = null;
beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);
});

afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});

it("render crypto data", async () => {
    const fakeCryptoProvider = {
        btc: { current: 123, history: [{ amount: 123, dateTime: '123' }] },
        eth: { current: 123, history: [{ amount: 123, dateTime: '123' }] },
        xrp: { current: 123, history: [{ amount: 123, dateTime: '123' }] },
    };
    jest.spyOn(global, "fetch").mockImplementation(() =>
        Promise.resolve({
            json: () => Promise.resolve(fakeCryptoProvider)
        })
    );

    await act(async () => {
        render(<Dashboard />, container);
    });

    expect(container.querySelector("BITSOAmount").textContent).toContain(fakeCryptoProvider.btc.current);

    global.fetch.mockRestore();
});