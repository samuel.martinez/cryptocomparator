import React, { useState, useEffect, useRef } from 'react';
import { useDashboard } from '../../contexts/dashboard/state';
import { getBitlem, getCoinRex, getBitso, getExchangeRate } from '../../contexts/dashboard/action';
import './dashboard.css';


function Dashboard() {
    const [amount, setAmount] = useState(0);
    const [amountTocryptos, setAmountTocryptos] = useState({ bitso: 0, coinrex: 0, bitlem: 0 });
    const [tab, setTab] = useState('btc');
    const [dashboardState, dashboardDispatch] = useDashboard();
    const { bitso, coinrex, bitlem, exchangeRate } = dashboardState;

    const handleConvert = () => {
        amount > 0 && setAmountTocryptos({
            bitso: amount / bitso[0]?.[tab]?.current,
            coinrex: amount / coinrex[0]?.[tab]?.current,
            bitlem: amount / bitlem[0]?.[tab]?.current
        })

    }

    useInterval(() => {
        if (exchangeRate.MXN) {
            getBitso(dashboardDispatch, exchangeRate?.MXN);
            getCoinRex(dashboardDispatch, exchangeRate?.MXN);
            getBitlem(dashboardDispatch, exchangeRate?.MXN);
        }
    }, 15000);

    useEffect(() => {
        if (exchangeRate.MXN) {
            getBitso(dashboardDispatch, exchangeRate?.MXN);
            getCoinRex(dashboardDispatch, exchangeRate?.MXN);
            getBitlem(dashboardDispatch, exchangeRate?.MXN);
        }
    }, [exchangeRate]);

    useEffect(() => {
        getExchangeRate(dashboardDispatch)
    }, []);

    useEffect(() => {
        handleConvert();
    }, [tab]);

    const positionFlag = (current, prev) => {
        if (current > prev) {
            return <i className="material-icons up amount">arrow_drop_up</i>
        } else if (current < prev) {
            return <i className="material-icons down amount">arrow_drop_down</i>
        } else {
            return null
        }

    }

    const currencyCompareSection = (currentAmount, history = [], currencyName) => <div id={`${currencyName}Amount`} className="divTableCell">
        <div className='amount'>
            {positionFlag(currentAmount, history[0]?.amount)} {currentAmount}
        </div>
        <div className='provider'>{currencyName}</div>
        <div className="divTableDetail">
            <div className="divTableBody">
                {history?.map((data, idx) => {
                    return (
                        <div id={idx} className="divTableRow">
                            <div className="divTableCellDetail">
                                {data.dateTime}
                            </div>
                            < div className="divTableCellDetail">
                                {data.amount}
                            </div>
                        </div>
                    )
                }
                )}
            </div>
        </div>
    </div>


    return (
        <div className='container'>
            <div className='dashboard'>
                <div className="tab">
                    <button id='btc_tab' className={tab === 'btc' ? 'selected' : ''} onClick={() => setTab('btc')}>BTC</button>
                    <button id='eth_tab' className={tab === 'eth' ? 'selected' : ''} onClick={() => setTab('eth')}>ETH</button>
                    <button id='xrp_tab' className={tab === 'xrp' ? 'selected' : ''} onClick={() => setTab('xrp')}>XRP</button>
                </div>

                <div className="tabcontent">
                    {tab === 'btc' && <div id="btc" className="tabsection">
                        <div className="divTable">
                            <div className="divTableBody">
                                <div className="divTableRow">
                                    {currencyCompareSection(bitso[0]?.btc?.current, bitso[0]?.btc?.history, 'BITSO')}
                                    {currencyCompareSection(coinrex[0]?.btc?.current, coinrex[0]?.btc?.history, 'COINREX')}
                                    {currencyCompareSection(bitlem[0]?.btc?.current, bitlem[0]?.btc?.history, 'BITLEM')}
                                </div>
                            </div>
                        </div></div>}

                    {tab === 'eth' && <div id="eth" className="tabcolumn">
                        <div className="divTable">
                            <div className="divTableBody">
                                <div className="divTableRow">
                                    {currencyCompareSection(bitso[0]?.eth?.current, bitso[0]?.eth?.history, 'BITSO')}
                                    {currencyCompareSection(coinrex[0]?.eth?.current, coinrex[0]?.eth?.history, 'COINREX')}
                                    {currencyCompareSection(bitlem[0]?.eth?.current, bitlem[0]?.eth?.history, 'BITLEM')}
                                </div>
                            </div>
                        </div>
                    </div>}

                    {tab === 'xrp' && <div id="xrp" className="tabcolumn">
                        <div className="divTable">
                            <div className="divTableBody">
                                <div className="divTableRow">
                                    {currencyCompareSection(bitso[0]?.xrp?.current, bitso[0]?.xrp?.history, 'BITSO')}
                                    {currencyCompareSection(coinrex[0]?.xrp?.current, coinrex[0]?.xrp?.history, 'COINREX')}
                                    {currencyCompareSection(bitlem[0]?.xrp?.current, bitlem[0]?.xrp?.history, 'BITLEM')}
                                </div>
                            </div>
                        </div>
                    </div>}
                </div>
                <div className='convert'>
                    <div className="divTable">
                        <div className="divTableBody">
                            <div className="divTableRow">
                                <div className="divTableCellDetail">
                                    <label htmlFor="amount"><b>Calculate into MXN</b></label>
                                    <input type="number" id='amount' name='amount' value={amount} onChange={(e) => setAmount(Number(e.target.value))} onBlur={handleConvert} />
                                </div>
                                <div className="divTableCell">
                                    <div>BITSO</div> <div>{amountTocryptos?.bitso}</div>
                                </div>
                                <div className="divTableCell">
                                    <div>COINREX</div> <div> {amountTocryptos?.coinrex}</div>
                                </div>
                                <div className="divTableCell">
                                    <div>BITLEM</div> <div> {amountTocryptos?.bitlem}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        </div >
    )
}

function useInterval(callback, delay) {
    const savedCallback = useRef();

    useEffect(() => {
        savedCallback.current = callback;
    }, [callback]);

    useEffect(() => {
        function tick() {
            savedCallback.current();
        }
        if (delay !== null) {
            let id = setInterval(tick, delay);
            return () => clearInterval(id);
        }
    }, [delay]);
}
export default Dashboard;