import './navBar.css';
import { useUser } from '../../contexts/user/state';
import { useHistory } from "react-router-dom";

const NavBar = () => {
    const [userState] = useUser();
    const { user } = userState;
    let history = useHistory();

    const logout = () => {
        localStorage.clear();
        history.push('/');
        history.go();
    }

    return (
        <div className="topnav">
            <a href="/dashboard">Crypto Comparator</a>
            <div className="user-container">
                {user.firstName && `Hello, ${user.firstName} ${user.lastName} | ${user.email} | ${user.phoneNumber}`}
                <button id='logout' className='logoutButton' onClick={logout}>Logout</button>
            </div>

        </div>
    )
}

export default NavBar