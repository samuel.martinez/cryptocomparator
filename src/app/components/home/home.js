import { React, useState, useEffect } from 'react';
import './home.css';
import { useUser } from '../../contexts/user/state';
import { setUser } from '../../contexts/user/action';
import { useHistory } from "react-router-dom";

function Home() {
    const [form, setForm] = useState({});
    const [userState, userDispatch] = useUser();
    const { user } = userState;
    let history = useHistory();

    useEffect(() => {
        user.firstName && history.push('/dashboard')
    }, [user, history])



    const handleChange = (event) => {
        setForm(form => ({ ...form, [event.target.name]: event.target.value }));
    }

    const handleSubmit = (event) => {
        setUser(userDispatch, form);
        event.preventDefault();
    }

    return (
        <div className="container">
            <h1>Welcome!</h1>
            <div>
                <form onSubmit={handleSubmit}>
                    <label htmlFor="firstName"><b>FirstName</b></label>
                    <input type="text" placeholder="Enter First Name" id='firstName' name='firstName' value={form.firstName} onChange={handleChange} required />

                    <label htmlFor="lastName"><b>LastName</b></label>
                    <input type="text" placeholder="Enter Last Name" id='lastName' name='lastName' value={form.lastName} onChange={handleChange} required />

                    <label htmlFor="email"><b>Email</b></label>
                    <input type="email" placeholder="Enter Email" name="email" id="email" required value={form.email} onChange={handleChange} />

                    <label htmlFor="phoneNumber"><b>Phone Number</b></label>
                    <input type="tel" placeholder="Enter Phone Number" id='phoneNumber' name='phoneNumber' value={form.phoneNumber} onChange={handleChange} required pattern="[0-9]{10}" />

                    <input type="submit" className="submitbtn" value="Submit" />
                </form>
            </div>
        </div>
    )
}

export default Home;