import { React } from 'react'

const CharacterNotSelected = (props) => {
    return (
        <div>
            {props.data?.id ? <div id={props.data.id}>
                <img src={props.data.image} />
                <h3>{`Status: ${props.data.status}`}</h3>
                <h3>{`Species: ${props.data.species}`}</h3>
                <h3>{`Gender: ${props.data.gender}`}</h3>
                <h3>{`Origin Name: ${props.data.origin.name}`}</h3>
            </div>:
            <div>
                <img src='https://rickandmortyapi.com/api/character/avatar/19.jpeg' />
                <h3>Not selected</h3>
                
            </div>
            }
        </div>
    )

}

export default CharacterNotSelected;