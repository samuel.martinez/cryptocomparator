import { React, useState, useEffect } from 'react';
import { useSearch } from '../../contexts/search/state';
import { searchCharacter } from '../../contexts/search/action';
import Character from './character'
import CharacterNotSelected from './characterNotSelected'

function Dashboard() {
    const [characterName, setCharacterName] = useState('');
    const [characterSelected, setCharacterSelected] = useState({});
    const [searchState, dispatch] = useSearch();
    const { characters } = searchState;

    const handleChange = (event) => {
        setCharacterName(event.target.value);
    }

    const handleSubmit = (event) => {
        searchCharacter(dispatch, characterName);
        event.preventDefault();
    }

    return (
        <div className="container">
            <h1>Welcome!</h1>
            <div>
                <form onSubmit={handleSubmit}>
                    <input type="text" placeholder="Name of Character" id='name' name='name' value={characterName} onChange={handleChange} />

                    <input type="submit" className="submitbtn" value="Search" />
                </form>
            </div>
            <div>
                <CharacterNotSelected data={characterSelected}/>
                {characters?.map(character => <Character data={character} setCharacterSelected={setCharacterSelected} />)}
            </div>
        </div>

    )
}

export default Dashboard;