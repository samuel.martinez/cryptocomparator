

const Character = (props) => {
    return (
        <div onClick={() => props.setCharacterSelected(props.data)} id={props.data.id} >
            <img src={props.data.image} />
            {props.data.name}
        </div>
    )

}

export default Character;