export const searchCharacter = async (dispatch, characterName) => {

    await fetch(`https://rickandmortyapi.com/api/character${characterName && `?name=${characterName}`}`)
        .then(response => {
            return response.json()
        }
        ).then(body => {
            dispatch({ type: "GET_CHARACTERS", payload: body.results })
            });;
}
