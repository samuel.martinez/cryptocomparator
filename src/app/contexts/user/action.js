export const setUser = (dispatch, user) => {
    localStorage.setItem('user_data', JSON.stringify(user));
    dispatch({ type: "SET_USER", payload: user });
}
