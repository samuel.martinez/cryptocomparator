import React, { useContext, useReducer } from "react";
import { UserContext } from "./context";
import UserReducer from "./reducer";

export const useUser = () => {
    const { state, dispatch } = useContext(UserContext);
    return [state, dispatch];
};

export const UserState = ({ children }) => {
    const initialState = {
        user: localStorage.getItem('user_data') ? JSON.parse(localStorage.getItem('user_data')) : {},
    };

    const [state, dispatch] = useReducer(UserReducer, initialState);

    return (
        <UserContext.Provider
            value={{
                state: state,
                dispatch: dispatch
            }}
        >
            {children}
        </UserContext.Provider>
    );
};