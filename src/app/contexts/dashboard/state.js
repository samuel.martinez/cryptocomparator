import React, { useContext, useReducer } from "react";
import { DashboardContext } from "./context";
import DashboardReducer from "./reducer";

export const useDashboard = () => {
    const { state, dispatch } = useContext(DashboardContext);
    return [state, dispatch];
};

export const DashboardState = ({ children }) => {
    const initialState = {
        bitso: [],
        coinrex: [],
        bitlem: [],
        exchangeRate: {}
    };

    const [state, dispatch] = useReducer(DashboardReducer, initialState);

    return (
        <DashboardContext.Provider
            value={{
                state: state,
                dispatch: dispatch
            }}
        >
            {children}
        </DashboardContext.Provider>
    );
};