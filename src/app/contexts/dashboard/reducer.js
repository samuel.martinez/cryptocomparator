const Dashboard = (state, action) => {
    switch (action.type) {
      case "SET_CC":
        return {
          ...state,
          bitso: action.payload
        };
      case "SET_CR":
        return {
          ...state,
          coinrex: action.payload
        };
      case "SET_SG":
        return {
          ...state,
          bitlem: action.payload
        };
      case "SET_EXCHANGE":
        return {
          ...state,
          exchangeRate: action.payload
        };
      default:
        return state;
    }
  };

export default Dashboard;