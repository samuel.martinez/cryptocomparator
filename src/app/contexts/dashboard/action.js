import axios from "axios";

const timeElapsed = Date.now();
const today = new Date(timeElapsed);

export const getBitso = async (dispatch, exchangeRate) => {
    await axios
        .get(`https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC,ETH,XRP&tsyms=USD`)
        .then(res => {
            let history = localStorage.getItem('ctyptoCompare') ? JSON.parse(localStorage.getItem('ctyptoCompare')) : { btc: [], eth: [], xrp: [] };
            const result = [{
                btc: { current: (res.data.BTC.USD * exchangeRate).toFixed(2), history: history.btc },
                eth: { current: (res.data.ETH.USD * exchangeRate).toFixed(2), history: history.eth },
                xrp: { current: (res.data.XRP.USD * exchangeRate).toFixed(2), history: history.xrp }
            }]
            history['btc'].unshift({ "amount": (res.data.BTC.USD * exchangeRate).toFixed(2), "dateTime": today.toUTCString() })
            history['eth'].unshift({ "amount": (res.data.ETH.USD * exchangeRate).toFixed(2), "dateTime": today.toUTCString() })
            history['xrp'].unshift({ "amount": (res.data.XRP.USD * exchangeRate).toFixed(2), "dateTime": today.toUTCString() })

            localStorage.setItem('ctyptoCompare', JSON.stringify(history));

            dispatch({
                type: "SET_CC",
                payload: result
            });
        })
        .catch(error => {
            console.log(error);
        });
};

export const getCoinRex = async (dispatch, exchangeRate) => {

    await axios
        .get(`https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&ids=bitcoin,ethereum,ripple`)
        .then(res => {
            let history = localStorage.getItem('coingecko') ? JSON.parse(localStorage.getItem('coingecko')) : { btc: [], eth: [], xrp: [] };

            const result = [{
                btc: { current: (res.data[0].current_price * exchangeRate).toFixed(2), history: history.btc },
                eth: { current: (res.data[1].current_price * exchangeRate).toFixed(2), history: history.eth },
                xrp: { current: (res.data[2].current_price * exchangeRate).toFixed(2), history: history.xrp }
            }]

            history['btc'].unshift({ "amount": (res.data[0].current_price * exchangeRate).toFixed(2), "dateTime": today.toUTCString() })
            history['eth'].unshift({ "amount": (res.data[1].current_price * exchangeRate).toFixed(2), "dateTime": today.toUTCString() })
            history['xrp'].unshift({ "amount": (res.data[2].current_price * exchangeRate).toFixed(2), "dateTime": today.toUTCString() })

            localStorage.setItem('coingecko', JSON.stringify(history));

            dispatch({
                type: "SET_CR",
                payload: result
            });
        })
        .catch(error => {
            console.log(error);
        });
};

export const getBitlem = async (dispatch, exchangeRate) => {

    await axios
        .get(`https://public-api.stormgain.com/api/v1/ticker`)
        .then(res => {

            let history = localStorage.getItem('stormgain') ? JSON.parse(localStorage.getItem('stormgain')) : { btc: [], eth: [], xrp: [] };

            const result = [{
                btc: { current: (res.data.BTC_USDT.last_price * exchangeRate).toFixed(2), history: history.btc },
                eth: { current: (res.data.ETH_USDT.last_price * exchangeRate).toFixed(2), history: history.eth },
                xrp: { current: (res.data.XRP_USDT.last_price * exchangeRate).toFixed(2), history: history.xrp }
            }]

            history['btc'].unshift({ "amount": (res.data.BTC_USDT.last_price * exchangeRate).toFixed(2), "dateTime": today.toUTCString() })
            history['eth'].unshift({ "amount": (res.data.ETH_USDT.last_price * exchangeRate).toFixed(2), "dateTime": today.toUTCString() })
            history['xrp'].unshift({ "amount": (res.data.XRP_USDT.last_price * exchangeRate).toFixed(2), "dateTime": today.toUTCString() })

            localStorage.setItem('stormgain', JSON.stringify(history));

            dispatch({
                type: "SET_SG",
                payload: result
            });
        })
        .catch(error => {
            console.log(error);
        });
};

export const getExchangeRate = async dispatch => {

    await axios
        .get(`https://api.exchangerate.host/latest?base=USD&symbols=MXN&places=2`)
        .then(res =>
            dispatch({
                type: "SET_EXCHANGE",
                payload: res.data.rates
            })
        )
        .catch(error => {
            console.log(error);
        });
};
