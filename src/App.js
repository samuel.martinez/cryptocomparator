import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Dashboard from './app/components/search/dashboard';
import { SearchState } from "./app/contexts/search/state";

function App() {
  return (
    <Router>
      <div>
          <SearchState>
            <Switch>
              <Route exact path="/">
                <Dashboard />
              </Route>
            </Switch>
          </SearchState>
      </div>
    </Router>
  );
}

export default App;
